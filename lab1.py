import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

data = pd.read_excel("Cell-Cycle-Set.xlsx")
display(data)

plt.hist(data['mean_RNA_G1'])
plt.hist(data['mean_protein_G1'])
plt.xlabel("mRNA in G1 phase")
plt.ylabel("Occurences")
plt.title("RNA vs protein levels in the G1 stage")
plt.show()
plt.hist(data['mean_RNA_S'])
plt.hist(data['mean_protein_S'])
plt.xlabel("mRNA in S phase")
plt.ylabel("Occurences")
plt.title("RNA vs protein levels in the S stage")
plt.show()
plt.hist(data['mean_RNA_G2'])
plt.hist(data['mean_protein_G2'])
plt.xlabel("mRNA in G2 phase")
plt.ylabel("Occurences")
plt.title("RNA vs protein levels in the G2 stage")
plt.show()

data.corr()

plt.scatter(data['mean_RNA_G1'], data['mean_protein_G1'], color = 'red')
plt.xlabel("mean RNA G1")
plt.ylabel("mean protein G1")
plt.title("RNA vs protein levels in G1 phase")
sns.regplot(data['mean_RNA_G1'], data['mean_protein_G1'], color = 'red', ci=None)
plt.show()

plt.scatter(data['mean_RNA_S'], data['mean_protein_S'], color = 'blue')
plt.xlabel("mean RNA S")
plt.ylabel("mean protein S")
plt.title("RNA vs protein levels in S phase")
sns.regplot(data['mean_RNA_S'], data['mean_protein_S'], color= 'blue', ci=None)
plt.show()

plt.scatter(data['mean_RNA_G2'], data['mean_protein_G2'], color = 'green')
plt.xlabel("mean RNA G2")
plt.ylabel("mean protein G2")
plt.title("RNA vs protein levels in G2 phase")
sns.regplot(data['mean_RNA_G2'], data['mean_protein_G2'], color = 'green', ci=None)
plt.show()

#Lab2 - Exercise 1
df = pd.read_excel("Cell-Cycle-Set.xlsx")
df.dropna(subset=['GOBP'], inplace=True)
df = df[df['GOBP'].str.contains("cell cycle")]


plt.scatter(df['mean_RNA_G1'], df['mean_protein_G1'], color='red')
plt.xlabel("mean RNA G1")
plt.ylabel("mean protein G1")
plt.title("RNA vs protein levels in G1 phase for GOBP term 'cell-cycle'")
sns.regplot(df['mean_RNA_G1'], df['mean_protein_G1'], color='red', ci=None)
plt.show()

plt.scatter(df['mean_RNA_S'], df['mean_protein_S'], color='blue')
plt.xlabel("mean RNA S")
plt.ylabel("mean protein S")
plt.title("RNA vs protein levels in S phase for GOBP term 'cell-cycle'")
sns.regplot(df['mean_RNA_S'], df['mean_protein_S'], color='blue', ci=None)
plt.show()

plt.scatter(df['mean_RNA_G2'], df['mean_protein_G2'], color='green')
plt.xlabel("mean RNA G2")
plt.ylabel("mean protein G2")
plt.title("RNA vs protein levels in G2 phase for GOBP term 'cell-cycle'")
sns.regplot(df['mean_RNA_G2'], df['mean_protein_G2'], color='green', ci=None)
plt.show()

fig, ax = plt.subplots()

ax.scatter(df['mean_RNA_G1'], df['mean_protein_G1'], label='G1', color='red')
sns.regplot(df['mean_RNA_G1'], df['mean_protein_G1'], color='red', ci=None)
ax.scatter(df['mean_RNA_S'], df['mean_protein_S'], label='S', color='blue')
sns.regplot(df['mean_RNA_S'], df['mean_protein_S'], color='blue', ci=None)
ax.scatter(df['mean_RNA_G2'], df['mean_protein_G2'], label='G2', color='green')
sns.regplot(df['mean_RNA_G2'], df['mean_protein_G2'], color='green', ci=None)

ax.set_xlabel("Mean RNA level")
ax.set_ylabel("Mean protein level")
ax.set_title("RNA vs protein levels across the cell cycle with GOBP term: 'cell cycle'")
ax.legend(loc='upper left')

plt.show()

#Lab2 - Exercise 2
df = pd.read_excel("Cell-Cycle-Set.xlsx")
df.dropna(subset=['GOCC'], inplace=True)
df = df[df['GOCC'].str.contains("ribosome")]


plt.scatter(df['mean_RNA_G1'], df['mean_protein_G1'], color='red')
plt.xlabel("mean RNA G1")
plt.ylabel("mean protein G1")
plt.title("RNA vs protein levels in G1 phase with GOCC term: 'ribosome'")
sns.regplot(df['mean_RNA_G1'], df['mean_protein_G1'], color='red', ci=None)
plt.show()

plt.scatter(df['mean_RNA_S'], df['mean_protein_S'], color='blue')
plt.xlabel("mean RNA S")
plt.ylabel("mean protein S")
plt.title("RNA vs protein levels in S phase with GOCC term: 'ribosome'")
sns.regplot(df['mean_RNA_S'], df['mean_protein_S'], color='blue', ci=None)
plt.show()

plt.scatter(df['mean_RNA_G2'], df['mean_protein_G2'], color='green')
plt.xlabel("mean RNA G2")
plt.ylabel("mean protein G2")
plt.title("RNA vs protein levels in G2 phase with GOCC term: 'ribosome'")
sns.regplot(df['mean_RNA_G2'], df['mean_protein_G2'], color='green', ci=None)
plt.show()


fig, ax = plt.subplots()

ax.scatter(df['mean_RNA_G1'], df['mean_protein_G1'], label='G1', color='red')
sns.regplot(df['mean_RNA_G1'], df['mean_protein_G1'], color='red', ci=None)
ax.scatter(df['mean_RNA_S'], df['mean_protein_S'], label='S', color='blue')
sns.regplot(df['mean_RNA_S'], df['mean_protein_S'], color='blue', ci=None)
ax.scatter(df['mean_RNA_G2'], df['mean_protein_G2'], label='G2', color='green')
sns.regplot(df['mean_RNA_G2'], df['mean_protein_G2'], color='green', ci=None)

ax.set_xlabel("Mean RNA level")
ax.set_ylabel("Mean protein level")
ax.set_title("RNA vs protein levels across the cell cycle with GOCC term: 'ribosome'")
ax.legend(loc='upper left')

plt.show()

#Lab2 - Exercise 3
df = pd.read_excel("Cell-Cycle-Set.xlsx")
df.dropna(subset=['GOBP'], inplace=True)
df = df[df['GOBP'].str.contains("cell cycle")]

# split the strings in the 'GOBP' column into lists of words
df['GOBP'] = df['GOBP'].str.split()

# use itertools.chain to flatten the list of lists into a single list of words
import itertools
words = list(itertools.chain.from_iterable(df['GOBP']))

# count the frequency of each word
word_counts = pd.Series(words).value_counts()

# display the word counts
print(word_counts)

#Lab2 - Exercise 4 Part 1
df = pd.read_excel("Cell-Cycle-Set.xlsx")

# Calculate the differences in mRNA/protein levels across the cell cycle
df['delta_RNA_G1S'] = df['mean_RNA_S'] - df['mean_RNA_G1']
df['delta_RNA_SG2'] = df['mean_RNA_G2'] - df['mean_RNA_S']
df['delta_RNA_G2G1'] = df['mean_RNA_G1'] - df['mean_RNA_G2']
df['delta_protein_G1S'] = df['mean_protein_S'] - df['mean_protein_G1']
df['delta_protein_SG2'] = df['mean_protein_G2'] - df['mean_protein_S']
df['delta_protein_G2G1'] = df['mean_protein_G1'] - df['mean_protein_G2']

# Mean-center the differences
df['delta_RNA_G1S'] = df['delta_RNA_G1S'] - np.mean(df['delta_RNA_G1S'])
df['delta_RNA_SG2'] = df['delta_RNA_SG2'] - np.mean(df['delta_RNA_SG2'])
df['delta_RNA_G2G1'] = df['delta_RNA_G2G1'] - np.mean(df['delta_RNA_G2G1'])
df['delta_protein_G1S'] = df['delta_protein_G1S'] - np.mean(df['delta_protein_G1S'])
df['delta_protein_SG2'] = df['delta_protein_SG2'] - np.mean(df['delta_protein_SG2'])
df['delta_protein_G2G1'] = df['delta_protein_G2G1'] - np.mean(df['delta_protein_G2G1'])

# Variance scale the differences
df['delta_RNA_G1S'] = df['delta_RNA_G1S'] / np.std(df['delta_RNA_G1S'])
df['delta_RNA_SG2'] = df['delta_RNA_SG2'] / np.std(df['delta_RNA_SG2'])
df['delta_RNA_G2G1'] = df['delta_RNA_G2G1'] / np.std(df['delta_RNA_G2G1'])
df['delta_protein_G1S'] = df['delta_protein_G1S'] / np.std(df['delta_protein_G1S'])
df['delta_protein_SG2'] = df['delta_protein_SG2'] / np.std(df['delta_protein_SG2'])
df['delta_protein_G2G1'] = df['delta_protein_G2G1'] / np.std(df['delta_protein_G2G1'])

# GOBP
df.dropna(subset=['GOBP'], inplace=True)
df = df[df['GOBP'].str.contains("cell cycle")]


plt.scatter(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red')
plt.xlabel("Delta mRNA G1-S")
plt.ylabel("Delta protein G1-S")
plt.title("RNA vs protein differences in G1-S phase for GOBP term: 'cell-cycle'")
sns.regplot(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red', ci=None)
plt.show()

plt.scatter(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue')
plt.xlabel("Delta mRNA S-G2")
plt.ylabel("Delta protein S-G2")
plt.title("RNA vs protein differences in S-G2 phase for GOBP term: 'cell-cycle'")
sns.regplot(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue', ci=None)
plt.show()

plt.scatter(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green')
plt.xlabel("Delta mRNA G2-G1")
plt.ylabel("Delta protein G2-G1")
plt.title("RNA vs protein differences in G2-G1 phase for GOBP term: 'cell-cycle'")
sns.regplot(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green', ci=None)
plt.show()


fig, ax = plt.subplots()

ax.scatter(df['delta_RNA_G1S'], df['delta_protein_G1S'], label='G1-S', color='red')
sns.regplot(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red', ci=None)
ax.scatter(df['delta_RNA_SG2'], df['delta_protein_SG2'], label='S-G2', color='blue')
sns.regplot(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue', ci=None)
ax.scatter(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], label='G2-G1', color='green')
sns.regplot(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green', ci=None)

ax.set_xlabel("Delta mRNA")
ax.set_ylabel("Delta protein")
ax.set_title("RNA vs protein differences across the cell cycle for GOBP term: 'cell-cycle'")
ax.legend(loc='upper left')

plt.show()

#Lab2 - Exercise 4 Part 2
df = pd.read_excel("Cell-Cycle-Set.xlsx")

# Calculate the differences in mRNA/protein levels across the cell cycle
df['delta_RNA_G1S'] = df['mean_RNA_G1'] - df['mean_RNA_S']
df['delta_RNA_SG2'] = df['mean_RNA_S'] - df['mean_RNA_G2']
df['delta_RNA_G2G1'] = df['mean_RNA_G2'] - df['mean_RNA_G1']
df['delta_protein_G1S'] = df['mean_protein_G1'] - df['mean_protein_S']
df['delta_protein_SG2'] = df['mean_protein_S'] - df['mean_protein_G2']
df['delta_protein_G2G1'] = df['mean_protein_G2'] - df['mean_protein_G1']

# Mean-center the differences
df['delta_RNA_G1S'] = df['delta_RNA_G1S'] - np.mean(df['delta_RNA_G1S'])
df['delta_RNA_SG2'] = df['delta_RNA_SG2'] - np.mean(df['delta_RNA_SG2'])
df['delta_RNA_G2G1'] = df['delta_RNA_G2G1'] - np.mean(df['delta_RNA_G2G1'])
df['delta_protein_G1S'] = df['delta_protein_G1S'] - np.mean(df['delta_protein_G1S'])
df['delta_protein_SG2'] = df['delta_protein_SG2'] - np.mean(df['delta_protein_SG2'])
df['delta_protein_G2G1'] = df['delta_protein_G2G1'] - np.mean(df['delta_protein_G2G1'])

# Variance scale the differences
df['delta_RNA_G1S'] = df['delta_RNA_G1S'] / np.std(df['delta_RNA_G1S'])
df['delta_RNA_SG2'] = df['delta_RNA_SG2'] / np.std(df['delta_RNA_SG2'])
df['delta_RNA_G2G1'] = df['delta_RNA_G2G1'] / np.std(df['delta_RNA_G2G1'])
df['delta_protein_G1S'] = df['delta_protein_G1S'] / np.std(df['delta_protein_G1S'])
df['delta_protein_SG2'] = df['delta_protein_SG2'] / np.std(df['delta_protein_SG2'])
df['delta_protein_G2G1'] = df['delta_protein_G2G1'] / np.std(df['delta_protein_G2G1'])

# GOCC
df.dropna(subset=['GOCC'], inplace=True)
df = df[df['GOCC'].str.contains("ribosome")]

plt.scatter(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red')
plt.xlabel("Delta mRNA G1-S")
plt.ylabel("Delta protein G1-S")
plt.title("RNA vs protein differences in G1-S phase for GOCC term: 'ribosome'")
sns.regplot(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red', ci=None)
plt.show()

plt.scatter(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue')
plt.xlabel("Delta mRNA S-G2")
plt.ylabel("Delta protein S-G2")
plt.title("RNA vs protein differences in S-G2 phase for GOCC term: 'ribosome'")
sns.regplot(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue', ci=None)
plt.show()

plt.scatter(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green')
plt.xlabel("Delta mRNA G2-G1")
plt.ylabel("Delta protein G2-G1")
plt.title("RNA vs protein differences in G2-G1 phase for GOCC term: 'ribosome'")
sns.regplot(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green', ci=None)
plt.show()

fig, ax = plt.subplots()

ax.scatter(df['delta_RNA_G1S'], df['delta_protein_G1S'], label='G1-S', color='red')
sns.regplot(df['delta_RNA_G1S'], df['delta_protein_G1S'], color='red', ci=None)
ax.scatter(df['delta_RNA_SG2'], df['delta_protein_SG2'], label='S-G2', color='blue')
sns.regplot(df['delta_RNA_SG2'], df['delta_protein_SG2'], color='blue', ci=None)
ax.scatter(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], label='G2-G1', color='green')
sns.regplot(df['delta_RNA_G2G1'], df['delta_protein_G2G1'], color='green', ci=None)

ax.set_xlabel("Delta mRNA")
ax.set_ylabel("Delta protein")
ax.set_title("RNA vs protein differences across the cell cycle for GOCC term: 'ribosome'")
ax.legend(loc='upper left')

#Lab2 - Exercise 5