import numpy as np
import scipy
import matplotlib.pyplot as plt
# %matplotlib inline
from functools import partial
from scipy.stats import binom
import math
import itertools
from scipy.integrate import quad
from scipy.special import hyp1f1
from bisect import bisect
import seaborn as sns

#Exercise 1 Part 1 (Fisher-Wright Model)
rng = np.random.default_rng()

def psm(n, P, s, u, v):
    """We heavily reuse this"""
    ps = (1+s)*n/(P+n*s)
    return (1-v)*ps + u*(1-ps)

def updateFW(n, P, s, u, v):
    """Generate a new population of size P starting with n individuals of type B"""
    return rng.binomial(P, psm(n, P, s, u, v))

class FisherWright():
    """Simulate the Dynamics of the Fisher-Wright Model
    We create an iterator to this"""
    
    def __init__(self, param):
        "create a function with the parameter set apart form n"
        self.update = partial(updateFW, **param)
        
    def __iter__(self):
        n = self.n0
        yield n
        while True:
            n = self.update(n)
            yield n

    def __call__(self, n0):
        """initiate n0 and return iterator"""
        self.n0 = n0
        return iter(self)

param = {"P": 1000, "s": 0.01, "u": 0.0001, "v": 0.0001}

fisherWright = FisherWright(param)

T = 10000

def evolution(model, n0, T, transform=lambda x: x):
    """Run the model for T generations starting from n0
        transform the output of the model with default of doing nothing"""
    it = model(n0)                # create a new iterator
    return [(i, transform(next(it))) for i in range(T)]


evo = evolution(fisherWright, 0, T)

def dict_to_str(dic):
    return ", ".join([f"{key} = {value}" for (key,value) in param.items()])

plt.plot(*zip(*evo))        # weird trick to get matplotlib to plot a list of (x,y) pairs
plt.xlabel("Generation, t")
plt.ylabel("n(t)")
plt.title(dict_to_str(param))
plt.show()

"""Exercise 1 - Part 2 (Question 1 Explanation)

1.Under what conditions does trait B take over the population?

Trait B takes over the peopulation if either the forward mutation rate "u" is higher than the backwards mutation rate "v" by a non insignificant amount or if there is a balance in the forward/backward mutation rates but the mutation rate is neither too high as to create too much noise and neither too low as to make the likeleyhood of mutation insignificant. This is all true only if the population is sufficiently large.
"""

#Exercise 1 - Part 3 (Find generation number at n(t)=0.95P)
def takeover_generation(param_dict):
    
    model = FisherWright(param_dict)
    
    #initial generation number 0
    for gen_number, value in enumerate(model(0)):
        
        if(value >= 0.95 * param_dict['P']):
            return gen_number

#Exercise 1 - Part 4 (Question 2.1 Code)
def s_dependance():
    
    #the s value will be timesed by 10 on each iteration
    params = {'P': 10000, 's': 0.0001, 'u': 0.0001, 'v': 0.0001}
    
    #tuples of the s value paired with the takeover time
    output = []
    
    for i in range(500):
        
        #record the value of s and the takeover time
        output.append((params['s'], takeover_generation(params)))
        params['s'] += 0.0001
    
    plt.plot(*zip(*output))
    plt.xlabel("Selection pressure (s)")
    plt.ylabel("Takeover time")
    plt.title("Takover time dependence on the selection pressure (s)")
    plt.show()

s_dependance()

"""Exercise 1 - Part 5 (Question 2.1 Explanation)

We can see that there is a significant correlation between selection strength and takeover time. As selection strength increases, takeover time decreases. That being said, as selection strength increases the rate at which takeover time decreases slows down.
"""

#Exercise 1 - Part 6 (Question 2.2 Code)
def mutation_rate_dependance():
    
    #the u and v vals will be incremented by 0.00001 on each iteration
    params = {'P': 10000, 's': 0.01, 'u': 0.00001, 'v': 0.00001}
    
    #tuples of the m value paired with the takeover time
    output = []
    
    for i in range(155):
                
        #u and v will always be the same so just pick either
        output.append((params['u'], takeover_generation(params)))
        params['u'] += 0.00001
        params['v'] += 0.00001
    
    plt.plot(*zip(*output))
    plt.xlabel("Mutation rate (m), linear scale")
    plt.ylabel("Takeover time, log scale")
    plt.yscale("log")
    plt.title("Takover time dependence on the mutation rate")
    plt.show()
    #return output

output = mutation_rate_dependance()

"""Exercise 1 - Part 7 (Question 2.2 Explanation)

In the graph above we can see that mutation rates have a significant impact on take over time. If mutation rates are too low, B takes significantly longer to take over the population. In fact, if the population is not high enough there is a good chance that B never even gets a foothold, remanining almost non-existent in the population. However, the opposite is also true. While experimenting with both the code in the wiki and my own code, I have found that if the mutation rate is too high, a state of equillibrium is reached, and it never gets to n(t)=0.95P. In my code, this happens after around around 160 increments, when "u" and "v" reach around 0.00156. When this occurs, my code never stops executing. Now, this could have also been because of hardware limitations, but after experimenting with the mutation rates on the wiki I found that my initial hypothesis, was correct.
"""

#Exercise 1 - Part 8 (Question 2.3 Code)
def p_dependance():
    
    #the P value will be timesed by 10 on each iteration
    params = {'P': 100, 's': 0.01, 'u': 0.0001, 'v': 0.0001}
    
    #tuples of the P value paired with the takeover time
    output = []
    
    for i in range(15):
        
        #u and v will always be the same so just pick either
        output.append((params['P'], takeover_generation(params)))
        params['P'] *= 10
    
    plt.plot(*zip(*output))
    plt.xlabel("Population size (P), log scale")
    plt.ylabel("Takeover time, linear scale")
    #plt.yscale("log")
    plt.xscale("log")
    plt.title("How takover time varies depending on the population size")
    plt.show()

p_dependance()

"""Exercise 1 - Part 9 (Question 2.3 Explanation)

A larger population size causes a quicker takeover time of trait B, however only up to a certain threshold, a population larger than around 100,000 has little effect on decreasing the takeover time as you can see in the graph. Furthermore, there is significantly more variance in lower populations as discussed in the lectures.
"""

#Exercise 2
def average_output(model, n_runs):

    runs = []
    for i in range(n_runs):
        runs.append(list(model()))
        
    average = []
    for i in range(len(runs[0])):
        average.append((i, np.mean([runs[j][i][1] for j in range(n_runs)])))
    
    return average

single_run = evolution(fisherWright, 0, T)
multiple_runs = average_output(lambda: evolution(fisherWright, 0, T), 1000)

print("\nTHE RESULTS OF A SINGLE FISHER-WRIGHT MODEL RUN:")
plt.plot(*zip(*single_run))        
plt.xlabel("Generation, t")
plt.ylabel("n(t)")
plt.title(dict_to_str(param))
plt.show()

print("\nTHE AVERAGE OF 1000 FISHER-WRIGHT MODEL RUNS:")
plt.plot(*zip(*multiple_runs))      
plt.xlabel("Generation, t")
plt.ylabel("n(t)")
plt.title(dict_to_str(param))
plt.show()

#Exercise 3 - Part 1 (Question 1 Part 1)
# Build transition matrix
W = np.zeros((param["P"]+1, param["P"]+1))
for n in range(param["P"]+1):
    for n_prime in range(param["P"]+1):
        # Probability of transition from n to n_prime
        if n_prime == n:
            W[n_prime, n] = 1 - param["u"] - psm(n_prime, param["P"], param["s"], param["u"], param["v"])
        elif n_prime == n+1:
            W[n_prime, n] = param["u"]
        elif n_prime == n-1:
            W[n_prime, n] = psm(n_prime, param["P"], param["s"], param["u"], param["v"])
        else:
            W[n_prime, n] = param["v"]*n_prime/param["P"]
       
# Compute p(t+10) = W^10 * p(0)
p0 = np.zeros(param["P"]+1)
p0[0] = 1  # start with all individuals having trait A
pt_final = np.linalg.matrix_power(W, 10) @ p0

# Check that the columns of W sum up to 1
# They sum up to one because markov chain is deterministic, and sum of probabilities x,y = 1. (Probability of entering given nr of mutants from given nr of mutants)
column_sums = np.sum(W, axis=0)
print("Column sums of W:")
print(column_sums)

#Exercise 3 - Part 2 (Question 1 Part 2)
def create_transition_matrix(P, s, u, v):
    n = np.arange(0,P+1)
    p = psm(n,P,s,u,v)
    return np.array([binom.pmf(nn, P, p) for nn in n])

def column_nr_and_sums(matrix):
    col_nr = len(matrix)
    col_sum = 0
    for i in range(len(matrix[0])):
        col_sum += sum(matrix[j][i] for j in range(len(matrix)))
    return (col_nr, int(col_sum))

#Round the result to an integer as the result is the same except
#floats are slightly inaccurate
W = create_transition_matrix(**param)
cols, sums = column_nr_and_sums(W)
print("The number of columns were", cols)
print("And the sum of all column sums is", sums)

#Exercise 3 - Part 3 (Question 1 Part 3)
def check_different_params(num_configs):
    for i in range(num_configs):
        s = np.random.random() * 0.1
        P = int(np.random.random() * (10 ** 4))
        v = np.random.random() * 0.0015
        u = np.random.random() * 0.0015
        
        matrix = create_transition_matrix(P, s, u, v)
        
        # Print column sums
        column_sums = np.sum(matrix, axis=0)
        print(f"Column sums of W (s={s:.4f}, P={P}, v={v:.4f}, u={u:.4f}):")
        print(column_sums)
        
        # Print number of columns and sum of column sums
        col_nr, col_sum = column_nr_and_sums(matrix)
        print(f"The number of columns were {col_nr}")
        print(f"And the sum of all column sums is {col_sum}")

check_different_params(5)

#Exercise 3 - Part 4 (Question 2)
class MarkovChain():
    """Markov Chain Model for simulating the Fisher Wright Model"""
    
    def __init__(self, P, s, u, v):
        self.W = create_transition_matrix(P, s, u, v)
        self.n = range(P+1)
        self.P = P
        
    def __iter__(self):
        p = np.array(self.p0)
        yield p
        while True:
            p = self.W@p
            yield p
            
    def mean(self, p):
        return self.n@p
    
    #Initialiese array
    def __call__(self, n):
        self.p0 = np.zeros(self.P+1)
        self.p0[n] = 1
        return iter(self)

param = {"P": 1000, "s": 0.01, "u": 0.001, "v": 0.001}
#Fisher-Wright simulations
nr_runs = 100
#Generations
T = 1000

fisherWright = FisherWright(param)
markovChain = MarkovChain(**param)

#Modle, initail pop, generations, mean func
markov = evolution(markovChain, 0, T, markovChain.mean)
fisher = evolution(fisherWright, np.zeros(nr_runs), T, np.mean)

plt.plot(*zip(*markov), label="Markov Chain Model")
plt.plot(*zip(*fisher), label="Fisher Wright Model")
plt.xlabel("Number of Generations, t")
#Expected nr of individuals carrying allele
plt.ylabel("E[n(t)]")
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 3 - Part 5 (Question 3)
def iterator(model, n0, T):
    """Return the distribution of the Population After T iterations starting from n0"""
    it = model(n0)
    for _ in range(T-1):
        next(it)
    dist = next(it)
    return list(range(len(dist))), dist

T = 1000
# Use markovChain model
n, markovDist = iterator(markovChain, 0, T)

# Simulate 1000 runs
no_copies = 1000
_, fisherDist = iterator(fisherWright, np.zeros(no_copies), T)

plt.hist(fisherDist, 20, density=True)
plt.plot(n, markovDist, "k")
plt.xlabel(f"Number of type $B$ individuals, n({T})")
plt.ylabel(f"P[n({T})]")
plt.title(dict_to_str(param))
plt.show()

#Exercise 3 - Part 6 (Question 3)
#Show probability of mutant number by generation t
#Iterate to generation T
def iterator(model, n0, T):
    """Return the distribution of the Population After T iterations starting from n0"""
    it = model(n0)
    for _ in range(T-1):
        next(it)
    dist = next(it)
    return list(range(len(dist))), dist

T = 100
nr_mutants, markovDist = iterator(markovChain, 0, T)
no_runs = 1000
_, fisherDist = iterator(fisherWright, np.zeros(no_runs), T)

# Create x and y arrays for the histogram
x_hist, bins = np.histogram(fisherDist, bins=20, range=(0, no_runs), density=True)
y_hist = x_hist / np.sum(x_hist)

plt.bar(bins[:-1], y_hist, width=bins[1]-bins[0], label='MultiEvolution')
# Multiply by 50 to scale
plt.plot(nr_mutants, 50*markovDist, "k", label='Markov Chain')
plt.xlabel("Number of type $B$ individuals")
plt.ylabel("Probability of Mutants at t={}".format(T))
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 4 - Part 1 (Question 1)
class steadyStateDiffusion():
    """ Computes the diffusion approximation to the steady state"""
    
    def __init__(self, P, s, u, v):
        self.param = [P,s,u,v]
        self.P = P
        self.norm = 1
        self.norm, _ = scipy.integrate.quad(self.__call__, 0, P)
        self.norm *= 1

    def psm(self, x):
        _, s, u, v = self.param
        ps = (1+s)*x/(1+s*x)
        return ps*(1-v) + u*(1-ps)

    def integrand(self, x):
        p = self.psm(x)
        a = p-x
        bs = p*(1-p)/self.P
        return -2*a/bs

    def __call__(self, n):
        x = n/self.P
        p = self.psm(x)
        bs = p*(1-p)/self.P
        integral, _ = scipy.integrate.quad(self.integrand,x,0.5)
        return math.exp(integral)/(self.norm*bs)

param = {"P": 1000, "s": 0.01, "u": 0.001, "v": 0.001}

diffusion = steadyStateDiffusion(**param)

## Markov Solution after a long time
T= 1000
n, markovDist = iterator(markovChain, 0, T)

#fig = plt.figure(figsize=(12,8))
plt.plot(n, markovDist, "--r", linewidth=2, label="Markov")

diffResult = [diffusion(i) for i in n]

plt.plot(n, diffResult, label="Diffusion")

plt.xlabel("Number of type $B$ individuals, n(inf)")
plt.ylabel("P[n(inf)]")
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 4 - Part 2 (Question 1)
#Differantial equation, doesn't need to simulate reality, it can get probabilities for larger populations at any time
class steadyStateDiffusion():
    """ Computes the diffusion approximation to the steady state"""
    
    def __init__(self, P, s, u, v):
        self.param = [P,s,u,v]
        self.P = P
        self.norm = 1
        self.norm, _ = scipy.integrate.quad(self.__call__, 0, P)
        self.norm *= 1

    #proportion of mutants
    def psm(self, x):
        _, s, u, v = self.param
        ps = (1+s)*x/(1+s*x)
        return ps*(1-v) + u*(1-ps)

    def integrand(self, x):
        p = self.psm(x)
        #Expected value vs current value of mutants
        a = p-x
        #Variance
        bs = p*(1-p)/self.P
        return -2*a/bs

    #n - pop of mutants -> output ssd
    def __call__(self, n):
        #normalize
        x = n/self.P
        p = self.psm(x)
        bs = p*(1-p)/self.P
        integral, _ = scipy.integrate.quad(self.integrand,x,0.5)
        return math.exp(integral)/(self.norm*bs)

diffusion = steadyStateDiffusion(**param)
T= 1000
n, markovDist = iterator(markovChain, 0, T)

# Multiply by 100 to convert to percentage
markovDist_perc = [p * 50 for p in markovDist]
plt.plot(n, markovDist_perc, "--r", linewidth=2, label="Markov")

diffResult = [diffusion(i) for i in n]
diffResult_perc = [p * 50 for p in diffResult]
plt.plot(n, diffResult_perc, label="Diffusion")

plt.xlabel("Number of type $B$ individuals")
plt.ylabel("Probability of Mutants at t={}".format(T))
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 4 - Part 3 (Question 2)
#assume mutation rates small compared to population -> simplify equations
class steadyStateWeakApprox():
    """Computes the steady state using the approximation of weak mutation and selection"""
    #beta distribution = distribution of the waiting time between two consecutive mutations + normalisation + init
    def __init__(self, P, s, u, v):
        self.abc = 2*P*np.array([u, v, s])
        self.P = P
        a, b, c = self.abc
        self.norm = -math.log(P) - scipy.special.betaln(a,b) - math.log(scipy.special.hyp1f1(a, a+b, c))

    #nr of type B  (pop n)
    def __call__(self, n):
        x = n/self.P
        a, b, c = self.abc
        return np.exp((a-1)*np.log(x) + (b-1)*np.log(1-x) + c*x + self.norm)


# Compute distribution using the weak selection/mutation approximation
weakApprox = steadyStateWeakApprox(**param)
logFix = np.array(n[1:-1])    # ignore x=0 and x=1 as logarithms don't like this
plt.plot(logFix, weakApprox(logFix), ":", linewidth=4, label="Weak Approx")

# Plot Markov result
plt.plot(n, markovDist, "r--", linewidth=2, label="Markov")

# Plot the diffusion approximation result
plt.plot(n, diffResult, label="Diffusion")

plt.xlabel("Number of type $B$ individuals, n(inf)")
plt.ylabel("P[n(inf)]")
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 4 - Part 4 (Question 2)
class steadyStateWeakApprox():
    """Computes the steady state using the approximation of weak mutation and selection"""
    
    def __init__(self, P, s, u, v):
        self.abc = 2*P*np.array([u, v, s])
        self.P = P
        a, b, c = self.abc
        self.norm = -math.log(P) - scipy.special.betaln(a,b) - math.log(scipy.special.hyp1f1(a, a+b, c))
        
    def __call__(self, n):
        x = n/self.P
        a, b, c = self.abc
        return np.exp((a-1)*np.log(x) + (b-1)*np.log(1-x) + c*x + self.norm)

# Compute distribution using the weak selection/mutation approximation
weakApprox = steadyStateWeakApprox(**param)
logFix = np.array(n[1:-1])    # ignore x=0 and x=1 as logarithms don't like this
plt.plot(logFix, weakApprox(logFix)*50, ":", linewidth=4, label="Weak Approx")

# Plot Markov result
plt.plot(n, markovDist*50, "r--", linewidth=2, label="Markov")

# Plot the diffusion approximation result
plt.plot(n, diffResult*50, label="Diffusion")

plt.xlabel("Number of type $B$ individuals")
plt.ylabel("Probability of Mutants at t={}".format(T))
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 4 - Part 5 (Question 3)

param = {"P": 1000, "s": 0.1, "u": 0.01, "v": 0.01}

# Compute distribution using the weak selection/mutation approximation
weakApprox = steadyStateWeakApprox(**param)
logFix = np.array(n[1:-1])    # ignore x=0 and x=1 as logarithms don't like this
plt.plot(logFix, weakApprox(logFix), ":", linewidth=4, label="Weak Approx")

# Plot Markov result
plt.plot(n, markovDist, "r--", linewidth=2, label="Markov")

# Plot the diffusion approximation result
plt.plot(n, diffResult, label="Diffusion")

plt.xlabel("Number of type $B$ individuals, n(inf)")
plt.ylabel("P[n(inf)]")
plt.title(dict_to_str(param))
plt.legend()
plt.show()

# Compute distribution using the weak selection/mutation approximation
weakApprox = steadyStateWeakApprox(**param)
logFix2 = np.array(n[1:-1])    # ignore x=0 and x=1 as logarithms don't like this
plt.plot(logFix2, weakApprox(logFix2)*50, ":", linewidth=4, label="Weak Approx")

# Plot Markov result
plt.plot(n, markovDist*50, "r--", linewidth=2, label="Markov")

# Plot the diffusion approximation result
plt.plot(n, diffResult*50, label="Diffusion")

plt.xlabel("Number of type $B$ individuals")
plt.ylabel("Probability of Mutants at t={}".format(T))
plt.title(dict_to_str(param))
plt.legend()
plt.show()

#Exercise 5 - Part 1 (Question 1)
param = {"P": 1000, "s": 0.01, "u": 0.001, "v": 0.001}
class Population():
    """Container of Population"""
    
    def __init__(self, P, L):
        """create P genomes of length L initialised to 0 string"""
        self.pop = np.zeros((P,L), dtype=bool)
        self.P = P
        self.L = L
        
    def __getitem__(self, k):
        """return genome of individual k"""
        return self.pop[k,:]
    
    def __len__(self):
        """size of population"""
        return self.P
    
    def no_ones(self):
        """compute the number of 1's in each string"""
        return np.sum(self.pop, axis=1)
    
    def prob(self, s):
        """Compute the probability of seclecting each member"""
        F = self.no_ones()
        F -= max(F)            # prevent overflow
        w = pow(1.0+s, F)
        return w/sum(w)
    
    def choose_parents(self, n, s):
        """Choose n parents according to their fitness"""
        p = self.prob(s)
        cp = np.cumsum(p)
        r = np.random.rand(2*self.P)
        return [bisect(cp, rng.random()) for _ in range(n)]
    
    def crossover(self, m, f):
        """Create a child by choosing each allele from either parent with equal probability"""
        mask = np.random.randint(2,size=self.L)
        imask = 1 - mask
        return mask*self[f] + imask*self[m] 
        
    def recombine(self, s):
        """Generate a new population from 2 P parents selected according to their fitness"""
        n = self.choose_parents(2*self.P, s)
        newPop = Population(self.P, self.L)
        for m,f,c in zip(n[::2], n[1::2], range(self.P)):
            newPop.pop[c,:] = self.crossover(f,m)
        return newPop
    
    def selection(self, s):
        """Generate a new population asexually"""
        n = self.choose_parents(self.P, s)
        newPop = Population(self.P, self.L)
        for c, p in enumerate(n):
            newPop.pop[c,:] = self[p]
        return newPop
        
    def mutate(self, u):
        """Flip spins with probablity u
        It does this by drawing a random deviate representing the expected gap between mutations"""
        PL = self.P*self.L
        c = 1.0/math.log(1.0-u)
        flat = self.pop.view(dtype=bool).reshape(PL)
        s = int(c*math.log(np.random.rand()))
        while s < PL:
            flat[s] = 1-flat[s]
            s += int(c*math.log(np.random.rand()))
            
    def mutants_per_site(self):
        """We are interested in the frequency of mutations at each Site"""
        return np.sum(self.pop, axis=0)

    def __str__(self):
        """Create a nice sting showing the population of their fitness"""
        F = self.no_ones()
        s = ""
        for k in range(self.P):
            s += f"{k}: {1*self[k]} F={F[k]}\n"
        return s

#Exercise 5 - Part 2 (Question 1)
#L=genome length
class PopulationEvol():
    """Iterate Class to Evolve a Population"""
    
    def __init__(self, L, P, s, u):
        self.L = L
        self.P = P
        self.s = s
        self.u = u
        
    #Selection function not needed as it is asexual
    def __iter__(self):
        pop = Population(self.P, self.L)
        yield pop
        while True:
            pop = pop.recombine(self.s)
            pop.mutate(self.u)
            yield pop
            
    def __call__(self, dummy):
        return iter(self)

#mean nr of mutations per site
def meanFitness(pop):
    """Returns the mean number of mutations in the population"""
    return np.mean(pop.mutants_per_site())

# Run a Population of strings
paramME = {"L": 100, "P": 1000, "s": 0.01, "u": 0.001}
modelME = PopulationEvol(**paramME)
T = 1000
#Do 1k generations
evoME = evolution(modelME, None, T, meanFitness)

# Compare with Marov Model
param = {"P": 1000, "s": 0.01, "u": 0.001, "v": 0.001}
markovChain = MarkovChain(**param)
evoMarkov = evolution(markovChain, 0, T, markovChain.mean)

plt.plot(*zip(*evoME), label="Sexual Evolution")
plt.plot(*zip(*evoMarkov), label="Markov Model")
plt.xlabel("Number of Generations, T")
plt.ylabel("Mean Number of Mutations per Site")
plt.title(dict_to_str(paramME))
plt.legend()
plt.show()

#Exercise 5 - Part 3 (Question 2)
def plot_population(population):
    """Plot the population as a histogram showing the number of 1s at each site"""
    frequencies = np.sum(population.pop, axis=0)
    plt.bar(range(len(frequencies)), frequencies)
    plt.xlabel("Site")
    plt.ylabel("Number of individuals that have mutation")
    plt.show()

paramME = {"L": 100, "P": 1000, "s": 0.01, "u": 0.001}
pop_evol = PopulationEvol(**paramME)
for i, pop in enumerate(pop_evol):
    if i == 100:
        plot_population(pop)
        break

#Exercise 5 - Part 4 (Question 3)
class PopulationAsexualEvol():
    """Iterate Class to Evolve a Population"""
    
    def __init__(self, L, P, s, u):
        self.L = L
        self.P = P
        self.s = s
        self.u = u
        
    def __iter__(self):
        pop = Population(self.P, self.L)
        yield pop
        while True:
            pop = pop.selection(self.s)
            pop.mutate(self.u)
            yield pop
            
    def __call__(self, dummy):
        return iter(self)

# Run a Population of strings
paramMEA = {"L": 100, "P": 1000, "s": 0.01, "u": 0.001}
modelMEA = PopulationAsexualEvol(**paramMEA)
T = 1500
evoMEA = evolution(modelMEA, None, T, meanFitness)
# Compare with Marov Model
param = {"P": 1000, "s": 0.01, "u": 0.001, "v": 0.001}
markovChain = MarkovChain(**param)
evoMarkov = evolution(markovChain, 0, T, markovChain.mean)
plt.plot(*zip(*evoMEA), label="Asexual Evolution")
plt.plot(*zip(*evoMarkov), label="Markov Model")
plt.xlabel("Generation, t")
plt.ylabel("n(t)")
plt.title(dict_to_str(paramMEA))
plt.legend()
plt.show()