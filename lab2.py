!pip install hmmlearn
!pip install biopython
!pip install hmmlearn

import numpy as np
import os
import math
import random
import matplotlib.pyplot as plt
from hmmlearn import hmm
from Bio import SeqIO
from hmmlearn import hmm
from sklearn.preprocessing import LabelEncoder
from collections import Counter
from termcolor import colored

#Exercise 1 part 1
def needleman_wunsch(seq1, seq2, gap_penalty):
    # Determine which sequence is longer
    if len(seq1) >= len(seq2):
        long_seq = seq1
        short_seq = seq2
    else:
        long_seq = seq2
        short_seq = seq1

    # Create scoring matrix and initialize first row and column
    score_matrix = [[0 for j in range(len(long_seq)+1)] for i in range(len(short_seq)+1)]
    traceback_matrix = [[None for j in range(len(long_seq)+1)] for i in range(len(short_seq)+1)]
    for i in range(len(short_seq)+1):
        score_matrix[i][0] = gap_penalty * i
        traceback_matrix[i][0] = "↑"
    for j in range(len(long_seq)+1):
        score_matrix[0][j] = gap_penalty * j
        traceback_matrix[0][j] = "←"

    # Fill in the rest of the matrix using the Needleman-Wunsch algorithm
    for i in range(1, len(short_seq)+1):
        for j in range(1, len(long_seq)+1):
            # Calculate scores for possible alignments
            match_score = score_matrix[i-1][j-1] + score(short_seq[i-1], long_seq[j-1])
            delete_score = score_matrix[i-1][j] + gap_penalty
            insert_score = score_matrix[i][j-1] + gap_penalty
            # Choose the best score and fill in the matrix and traceback
            max_score = max(match_score, delete_score, insert_score)
            score_matrix[i][j] = max_score
            if max_score == match_score:
                traceback_matrix[i][j] = "↖"
            elif max_score == delete_score:
                traceback_matrix[i][j] = "↑"
            else:
                traceback_matrix[i][j] = "←"

    # Print the score matrix and traceback
    print("Score matrix:")
    for row in score_matrix:
        print(row)
    print("Traceback matrix:")
    for row in traceback_matrix:
        print(row)

    return score_matrix

def score(char1, char2):
    # Lookup score in BLOSUM50 matrix
    i = "ACDEFGHIKLMNPQRSTVWY".index(char1)
    j = "ACDEFGHIKLMNPQRSTVWY".index(char2)
    return blosum50[i][j]

# Load BLOSUM50 matrix from file
blosum50 = []
with open("blosum50.txt") as f:
    for line in f:
        blosum50.append([int(x) for x in line.split()])

# Example usage
matrix = needleman_wunsch("HEAGAWGHEE", "PAWHEAE", -8)

#Exercise 1 part 2
def needleman_wunsch(seq1, seq2, gap_penalty):
    # Determine which sequence is longer
    if len(seq1) >= len(seq2):
        long_seq = seq1
        short_seq = seq2
    else:
        long_seq = seq2
        short_seq = seq1

    # Create scoring matrix and initialize first row and column
    score_matrix = [[0 for j in range(len(long_seq)+1)] for i in range(len(short_seq)+1)]
    traceback_matrix = [[None for j in range(len(long_seq)+1)] for i in range(len(short_seq)+1)]
    for i in range(len(short_seq)+1):
        score_matrix[i][0] = gap_penalty * i
        traceback_matrix[i][0] = "↑"
    for j in range(len(long_seq)+1):
        score_matrix[0][j] = gap_penalty * j
        traceback_matrix[0][j] = "←"

    # Fill in the rest of the matrix using the Needleman-Wunsch algorithm
    for i in range(1, len(short_seq)+1):
        for j in range(1, len(long_seq)+1):
            # Calculate scores for possible alignments
            match_score = score_matrix[i-1][j-1] + score(short_seq[i-1], long_seq[j-1])
            delete_score = score_matrix[i-1][j] + gap_penalty
            insert_score = score_matrix[i][j-1] + gap_penalty
            # Choose the best score and fill in the matrix and traceback
            max_score = max(match_score, delete_score, insert_score)
            score_matrix[i][j] = max_score
            if max_score == match_score:
                traceback_matrix[i][j] = "↖"
            elif max_score == delete_score:
                traceback_matrix[i][j] = "↑"
            else:
                traceback_matrix[i][j] = "←"

    # Traceback to generate aligned sequences
    aligned_seq1 = ""
    aligned_seq2 = ""
    i = len(short_seq)
    j = len(long_seq)
    while i > 0 or j > 0:
        if traceback_matrix[i][j] == "←":
            aligned_seq1 = "-" + aligned_seq1
            aligned_seq2 = long_seq[j-1] + aligned_seq2
            j -= 1
        elif traceback_matrix[i][j] == "↑":
            aligned_seq1 = short_seq[i-1] + aligned_seq1
            aligned_seq2 = "-" + aligned_seq2
            i -= 1
        else:
            aligned_seq1 = short_seq[i-1] + aligned_seq1
            aligned_seq2 = long_seq[j-1] + aligned_seq2
            i -= 1
            j -= 1

    # Print the aligned sequences
    print(f"String1: {aligned_seq1}")
    print(f"String2: {aligned_seq2}")

    # Return the score matrix
    #return score_matrix

def score(char1, char2):
    # Lookup score in BLOSUM50 matrix
    # ARNDCQEGHILKMFPSTWYV
    x = "ARNDCQEGHILKMFPSTWYV"
    y = "ACDEFGHIKLMNPQRSTVWY"
    i = x.index(char1)
    j = x.index(char2)
    return blosum50[i][j]

# Load BLOSUM50 matrix from file
blosum50 = []
with open("blosum50.txt") as f:
    for line in f:
        blosum50.append([int(x) for x in line.split()])

# Example usage
matrix = needleman_wunsch("HEAGAWGHEE", "PAWHEAE", -8)
matrix2 = needleman_wunsch('SALPQPTTPVSSFTSGSMLGRTDTALTNTYSAL', 'PSPTMEAVTSVEASTASHPHSTSSYFATTYYHLY', -8)

#Exercise 2
def smith_waterman(seq1, seq2, gap_penalty):
    # Initialize scoring and traceback matrices
    score_matrix = [[0 for j in range(len(seq2)+1)] for i in range(len(seq1)+1)]
    traceback_matrix = [[None for j in range(len(seq2)+1)] for i in range(len(seq1)+1)]

    # Initialize variables to keep track of highest score and its position
    highest_score = 0
    highest_i, highest_j = 0, 0

    # Fill in the scoring and traceback matrices
    for i in range(1, len(seq1)+1):
        for j in range(1, len(seq2)+1):
            # Calculate scores for possible alignments
            match_score = score_matrix[i-1][j-1] + score(seq1[i-1], seq2[j-1])
            delete_score = score_matrix[i-1][j] + gap_penalty
            insert_score = score_matrix[i][j-1] + gap_penalty
            # Choose the best score and fill in the matrix and traceback
            score_matrix[i][j] = max(0, match_score, delete_score, insert_score)
            if score_matrix[i][j] == 0:
                traceback_matrix[i][j] = None
            elif score_matrix[i][j] == match_score:
                traceback_matrix[i][j] = "↖"
            elif score_matrix[i][j] == delete_score:
                traceback_matrix[i][j] = "↑"
            else:
                traceback_matrix[i][j] = "←"
            # Update highest score and its position
            if score_matrix[i][j] > highest_score:
                highest_score = score_matrix[i][j]
                highest_i, highest_j = i, j

    # Extract the aligned sequences by following the traceback from the highest score
    aligned_seq1, aligned_seq2 = "", ""
    i, j = highest_i, highest_j
    while traceback_matrix[i][j] is not None:
        if traceback_matrix[i][j] == "↖":
            aligned_seq1 = seq1[i-1] + aligned_seq1
            aligned_seq2 = seq2[j-1] + aligned_seq2
            i -= 1
            j -= 1
        elif traceback_matrix[i][j] == "↑":
            aligned_seq1 = seq1[i-1] + aligned_seq1
            aligned_seq2 = "-" + aligned_seq2
            i -= 1
        else:
            aligned_seq1 = "-" + aligned_seq1
            aligned_seq2 = seq2[j-1] + aligned_seq2
            j -= 1

    # Print the aligned sequences and the highest score
    print("Aligned sequence 1:", aligned_seq1)
    print("Aligned sequence 2:", aligned_seq2)
    print("Highest score:", highest_score)

    return score_matrix

def score(char1, char2):
    # Lookup score in BLOSUM50 matrix
    # ARNDCQEGHILKMFPSTWYV
    x = "ARNDCQEGHILKMFPSTWYV"
    y = "ACDEFGHIKLMNPQRSTVWY"
    i = x.index(char1)
    j = x.index(char2)
    return blosum50[i][j]

# Load BLOSUM50 matrix from file
blosum50 = []
with open("blosum50.txt") as f:
    for line in f:
        blosum50.append([int(x) for x in line.split()])

# Example usage
matrix = smith_waterman("HEAGAWGHEE", "PAWHEAE", -8)
matrix2 = smith_waterman('MQNSHSGVNQLGGVFVNGRPLPDSTRQKIVELAHSGARPCDISRILQVSNGCVSKILGRY', 'TDDECHSGVNQLGGVFVGGRPLPDSTRQKIVELAHSGARPCDISRI', -8)

#Exercise 3 - Done -> https://www.uniprot.org/align/clustalo-R20230224-155548-0750-4352111-p2m/overview
#Exercise 4
states = ['AT rich', 'CG rich']
start_prob = {'AT rich': 0.5, 'CG rich': 0.5}
transition_prob = {
    'AT rich': {'AT rich': 0.9998, 'CG rich': 0.0002},
    'CG rich': {'AT rich': 0.0003, 'CG rich': 0.9997}
}
emission_prob = {
    'AT rich': {'A': 0.2698, 'T': 0.3237, 'C': 0.2080, 'G': 0.1985},
    'CG rich': {'A': 0.2459, 'T': 0.2079, 'C': 0.2478, 'G': 0.2984}
}

def generate_sequence(length):
    # Initialize the state to the starting state
    state = random.choices(states, weights=[start_prob[state] for state in states])[0]
    sequence = ''
    # statess = ''
    for i in range(length):
        # Generate the next nucleotide based on the emission probabilities for the current state
        nucleotide = random.choices(['A', 'T', 'C', 'G'], weights=[emission_prob[state][nucleotide] for nucleotide in ['A', 'T', 'C', 'G']])[0]
        sequence += nucleotide
        # statess += state + ","
        # Transition to the next state based on the transition probabilities for the current state
        state = random.choices(states, weights=[transition_prob[state][next_state] for next_state in states])[0]
    # print (statess)
    return sequence

sequence = generate_sequence(100)
print(sequence)

#Exercise 5 - No Log
def viterbi_no_log(sequence):
    # Initialize the Viterbi matrix
    viterbi_matrix = [{state: (start_prob[state] * emission_prob[state][sequence[0]], None) for state in states}]
    # Iterate over the sequence
    for i in range(1, len(sequence)):
        # Initialize the current row of the Viterbi matrix
        current_row = {}
        # Iterate over the possible states at the current position
        for state in states:
            # Find the maximum probability and previous state for each possible transition
            max_prob, prev_state = max([(viterbi_matrix[i-1][prev_state][0] * transition_prob[prev_state][state] * emission_prob[state][sequence[i]], prev_state) for prev_state in states])
            # Add the maximum probability and previous state to the current row
            current_row[state] = (max_prob, prev_state)
        # Add the current row to the Viterbi matrix
        viterbi_matrix.append(current_row)
    # Find the maximum probability and corresponding state at the end of the sequence
    max_prob, max_state = max([(viterbi_matrix[-1][state][0], state) for state in states])
    # Initialize the most likely path with the final state
    most_likely_path = [max_state]
    # Iterate over the Viterbi matrix in reverse order to find the most likely path
    for i in range(len(viterbi_matrix)-1, 0, -1):
        # Find the previous state in the most likely path
        prev_state = viterbi_matrix[i][most_likely_path[-1]][1]
        # Add the previous state to the most likely path
        most_likely_path.append(prev_state)
    # Reverse the most likely path to get the correct order
    most_likely_path.reverse()
    return most_likely_path

def color_sequence(sequence, colors):
    colored_sequence = ''
    for i, char in enumerate(sequence):
        if colors[i] == 'CG rich':
            colored_sequence += '\033[32m' + char + '\033[0m'
        else:
            colored_sequence += '\033[31m' + char + '\033[0m'
    return colored_sequence

# Generate a sequence of length 100
sequence = generate_sequence(100)
# Find the most likely CG regions using the Viterbi algorithm
most_likely_path = viterbi_no_log(sequence)
# Print the most likely path and the corresponding nucleotide sequence
#print('Most likely path:', most_likely_path)
#print('Nucleotide sequence:', sequence)

colors = viterbi_no_log(sequence)
colored_sequence = color_sequence(sequence, colors)
print(colored_sequence)

# Plot the nucleotide sequence with CG-rich regions highlighted
fig, ax = plt.subplots(figsize=(10,1))
for i in range(len(sequence)):
    if most_likely_path[i] == 'CG rich':
        ax.axvspan(i, i+1, color='green', alpha=0.2)
    else:
        ax.axvspan(i, i+1, color='red', alpha=0.2)
    ax.text(i+0.5, 0.5, sequence[i], ha='center', va='center')
ax.set_xlim(0, len(sequence))
ax.set_ylim(0, 1)
ax.set_xticks([])
ax.set_yticks([])
plt.show()

#Exercise 5 - Log
def viterbi_log(sequence):
    # Initialize the Viterbi matrix
    viterbi_matrix = [{state: (math.log(start_prob[state]) + math.log(emission_prob[state][sequence[0]]), None) for state in states}]
    # Iterate over the sequence
    for i in range(1, len(sequence)):
        # Initialize the current row of the Viterbi matrix
        current_row = {}
        # Iterate over the possible states at the current position
        for state in states:
            # Find the maximum probability and previous state for each possible transition
            max_prob, prev_state = max([(viterbi_matrix[i-1][prev_state][0] + math.log(transition_prob[prev_state][state]) + math.log(emission_prob[state][sequence[i]]), prev_state) for prev_state in states])
            # Add the maximum probability and previous state to the current row
            current_row[state] = (max_prob, prev_state)
        # Add the current row to the Viterbi matrix
        viterbi_matrix.append(current_row)
    # Find the maximum probability and corresponding state at the end of the sequence
    max_prob, max_state = max([(viterbi_matrix[-1][state][0], state) for state in states])
    # Initialize the most likely path with the final state
    most_likely_path = [max_state]
    # Iterate over the Viterbi matrix in reverse order to find the most likely path
    for i in range(len(viterbi_matrix)-1, 0, -1):
        # Find the previous state in the most likely path
        prev_state = viterbi_matrix[i][most_likely_path[-1]][1]
        # Add the previous state to the most likely path
        most_likely_path.append(prev_state)
    # Reverse the most likely path to get the correct order
    most_likely_path.reverse()
    return most_likely_path

def color_sequence(sequence, colors):
    colored_sequence = ''
    for i, char in enumerate(sequence):
        if colors[i] == 'CG rich':
            colored_sequence += '\033[32m' + char + '\033[0m'
        else:
            colored_sequence += '\033[31m' + char + '\033[0m'
    return colored_sequence

# Generate a sequence of length 100
sequence = generate_sequence(100)
# Find the most likely CG regions using the Viterbi algorithm
most_likely_path = viterbi_log(sequence)
# Print the most likely path and the corresponding nucleotide sequence
#print('Most likely path:', most_likely_path)
#print('Nucleotide sequence:', sequence)

colors = viterbi_no_log(sequence)
colored_sequence = color_sequence(sequence, colors)
print(colored_sequence)

# Plot the nucleotide sequence with CG-rich regions highlighted
fig, ax = plt.subplots(figsize=(10,1))
for i in range(len(sequence)):
    if most_likely_path[i] == 'CG rich':
        ax.axvspan(i, i+1, color='green', alpha=0.2)
    else:
        ax.axvspan(i, i+1, color='red', alpha=0.2)
    ax.text(i+0.5, 0.5, sequence[i], ha='center', va='center')
ax.set_xlim(0, len(sequence))
ax.set_ylim(0, 1)
ax.set_xticks([])
ax.set_yticks([])
plt.show()

#Exercise 6
def read_file(filename, length):
    with open(filename, 'r') as f:
        if length == 0:
            sequence = f.read().replace('\n', '')
        else:
            sequence = f.read(length).replace('\n', '')
    return sequence

def color_sequence(sequence, colors):
    colored_sequence = ''
    for i, char in enumerate(sequence):
        if colors[i] == 'CG rich':
            colored_sequence += '\033[32m' + char + '\033[0m'
        else:
            colored_sequence += '\033[31m' + char + '\033[0m'
    return colored_sequence

sequence = read_file("phaseLambda.fasta.txt", 3000)
#sequence = generate_sequence(1000)
colors = viterbi_log(sequence)
colored_sequence = color_sequence(sequence, colors)
print(colored_sequence)