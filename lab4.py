import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.integrate import odeint
import random
import math

"""Question 1:

∅ → X at a rate 1

X → Y at rate 2

2 X + Y → 3 X at rate 0.02

X → ∅ at rate 0.04

-------------------------------------------------------------------------------

d[X]/dt = k1 - k2 * [X] - 2 * k3 * [X]^2 [Y] + 3 * k3 * [X]^2 [Y] - k4 [X]

d[X]/dt = 1 - 2 * [X] - 2 * 0.02 * [X]^2 [Y] + 3 * 0.02 * [X]^2 [Y] - 0.04 [X]

d[X]/dt = 1 + 0.06 * [X]^2 [Y] - 0.04 * [X]^2 [Y] - 0.04 [X] - 2 [X]

d[X]/dt = 1 + 0.02 * [X]^2 [Y] -  2.04 [X]

-------------------------------------------------------------------------------


d[Y]/dt = k2 * [X] - k3 * [X]^2 [Y]

d[Y]/dt = 2 * [X] - 0.02 * [X]^2 [Y]
"""

#Question 2 - Part 1
def odeSolver(vals, t):
    [x,y] = vals
    dxdt = 1 - (2.04 * x) + (0.02 * (x ** 2) * y)
    dydt = (2 * x) - (0.02 * (x ** 2) * y)
    return [dxdt, dydt]

#initially X = 0 and Y = 0
init = [0,0]

#solve the differential equations for 500 time units
results = odeint(odeSolver, init, range(500))

#pair the results with each of their timesteps
xvals = [(time, pair[0]) for time, pair in zip(range(500), results)]
yvals = [(time, pair[1]) for time, pair in zip(range(500), results)]

fig, ax = plt.subplots()
ax.plot(*zip(*xvals), "r--", label = "X")
ax.plot(*zip(*yvals), "b--", label = "Y")
ax.set_xlabel("Time units")
ax.set_ylabel("Molecule count")
ax.set_title("Solving the differential equation system over 500 time units")
ax.legend()

"""Question 2 - Part 2

Because there is no X or Y in the system initially, the term "0.02 * [X]^2 [Y]" becomes 0. This leads to an initial increase in X, whose creation rate is "1" while its dissapearance rate is 0.04. Y is initially not created as it is dependent on X. Very soon after however, the creation rate of Y(=2) reduces the X concentration leading to an exponential increase in Y. Once the concentration of Y becomes significant the term "0.02 * [X]^2 [Y]" also becomes more significant, roughly around 60 time units. This term is positive in d[X]/dt and negative in d[Y]/dt. This results in a quick drop in the concentration of Y and a quick increase in the concentration of X. This increase in X gets corrected for to some degree soon after, until X and Y reach a state of equillibrium, with the concentration of X resting around 27 and the concentration of Y resting around 3.
"""

#Question 3 - Part 1
# Reaction rates
k1 = 1
k2 = 2
k3 = 0.02
k4 = 0.04

# Initial concentrations
X = 0
Y = 0

# Simulation time and time step
t_max = 500
dt = 0.01

# Lists to store the time and concentration values
t_list = [0]
X_list = [X]
Y_list = [Y]

# Helper function to calculate the propensity functions
def propensities(X, Y):
    a1 = k1
    a2 = k2 * X
    a3 = k3 * X**2 * Y
    a4 = k4 * X
    return [a1, a2, a3, a4]

# Helper function to choose the next reaction to occur
def next_reaction(propensities):
    total_propensity = sum(propensities)
    r1 = random.uniform(0, 1)
    #Time until next reaction
    tau = -(1 / total_propensity) * (math.log(1 - r1))
    #random, which reaction will occur
    r2 = random.uniform(0, 1)
    i = 0
    while (r2 * total_propensity > propensities[i] and i < len(propensities) - 1):
        r2 -= propensities[i] / total_propensity
        i += 1
    return i, tau

# Simulation loop
t = 0
while t < t_max:
    # Calculate propensities
    a1, a2, a3, a4 = propensities(X, Y)
    prop = [a1, a2, a3, a4]
    
    # Choose next reaction
    i, tau = next_reaction(prop)
    
    # Update concentrations
    if i == 0:
        X += 1
    elif i == 1:
        X -= 1
        Y += 1
    elif i == 2:
        X -= 2
        Y -= 1
        X += 3
    else:
        X -= 1
    
    # Update time
    t += tau
    
    # Append results to lists
    t_list.append(t)
    X_list.append(X)
    Y_list.append(Y)

# Plot results
fig, ax = plt.subplots()
ax.plot(t_list, X_list, 'r--', label='X')
ax.plot(t_list, Y_list, 'b--', label='Y')
ax.set_xlabel('Time')
ax.set_ylabel('Concentration')
ax.legend()
ax.set_title('Gillespie algorithm simulation')
plt.show()

"""Question 3 - Part 2

Although the graph does not show the same clear, smooth lines shown by our solutions from the ODE solver, we can recognise the same trends namely the Y population initially growing much faster than X and then immediately decreasing to a smaller amount, followed by X and Y remaining in this "equilibrium" state with the Y population staying smaller than the X population. The equilibrium state is clearer for the Y molecules than the X molecules, however this is to be expected due to the fact the general X population stays larger so any slight fluctuations are much more apparent in the data. Additionaly, the random component introduces a few more of these initial fluctuations, throughout the runtime, where the Y concentration grows exponentially, then decreases exponentially, finally reaching an equillibium.
"""